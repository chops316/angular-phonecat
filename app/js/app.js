'use strict';

/* App Module */

var phonecatApp = angular.module('phonecatApp', [
  'ngRoute',
  'phonecatAnimations',
  'phonecatControllers',
  'phonecatDirectives',
  'phonecatFilters',
  'phonecatServices',
  'ui.bootstrap'

]);

phonecatApp.config(['$routeProvider',
  function($routeProvider) {
    $routeProvider.
     when('/clients', {
      templateUrl: 'partials/client-views/client-list.html',
      controller: 'ClientTableCtrl'
    }).when('/clients/:clientId', {
      templateUrl: 'partials/client-views/client-detail.html',
      controller: 'ClientDetailCtrl'
    }).when('/sign-up', {
      templateUrl: 'partials/sign-up.html',
      controller: 'SignInCtrl'
    }).
    // Trackr
    when('/trackr/nutrition',{
      templateUrl: 'partials/trackr/nutrition-tracker.html'
    }).
    when('/trackr/workout',{
      templateUrl: 'partials/trackr/workout-tracker.html'
    }).
    when('/trackr/habit',{
      templateUrl: 'partials/trackr/habit-tracker.html'
    }).
    // Buildr
    when('/buildr/nutrition',{
      templateUrl: 'partials/buildr/meal-builder.html'
    }).
    when('/buildr/workout',{
      templateUrl: 'partials/buildr/workout-builder.html'
    }).
    when('/buildr/habit',{
      templateUrl: 'partials/buildr/habit-builder.html'
    }).
    // Schedulr
    when('/schedulr/nutrition',{
      templateUrl: 'partials/schedulr/meal-scheduler.html'
    }).
    when('/schedulr/workout',{
      templateUrl: 'partials/schedulr/workout-scheduler.html'
    }).
    when('/schedulr/habit',{
      templateUrl: 'partials/schedulr/habit-scheduler.html'
    }).
    // Overviews
    when('/team-overview', {
      templateUrl: 'partials/overviews/team-overview.html'
    }).
    when('/client-overview', {
      templateUrl: 'partials/overviews/client-overview.html'
    }).
    when('/business-overview', {
      templateUrl: 'partials/overviews/business-overview.html'
    }).
    // Messenger
    when('/messenger', {
      templateUrl: 'partials/messenger/messenger.html'
    }).
    when('/messenger/clients', {
      templateUrl: 'partials/messenger/client-messenger.html'
    }).
    when('/messenger/team', {
      templateUrl: 'partials/messenger/team-messenger.html'
    }).
    when('/inbox', {
      templateUrl: 'partials/messenger/inbox.html'
    }).
    // Leads
    when('/leads/new', {
      templateUrl: 'partials/leads/new-leads.html'
    }).
    when('/leads/active', {
      templateUrl: 'partials/leads/active-leads.html'
    }).
    when('/leads/lost', {
      templateUrl:'partials/leads/lost-leads.html'
    }).
    // Default
    otherwise({
      redirectTo: 'login.html'
      });
  }]);

phonecatApp.constant('AUTH_EVENTS', {
  loginSuccess: 'auth-login-success',
  loginFailed: 'auth-login-failed',
  logoutSuccess: 'auth-logout-success',
  sessionTimeout: 'auth-session-timeout',
  notAuthenticated: 'auth-not-authenticated',
  notAuthorized: 'auth-not-authorized'
});

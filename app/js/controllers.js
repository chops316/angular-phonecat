'use strict';

/* Controllers */

var phonecatControllers = angular.module('phonecatControllers', []);

phonecatControllers.controller('PhoneListCtrl', ['$scope', 'Phone',
  function($scope, Phone) {
    $scope.phones = Phone.query();
    $scope.orderProp = 'age';
  }]);

phonecatControllers.controller('PhoneDetailCtrl', ['$scope', '$routeParams', 'Phone',
  function($scope, $routeParams, Phone) {
    $scope.phone = Phone.get({phoneId: $routeParams.phoneId}, function(phone) {
      $scope.mainImageUrl = phone.images[0];
    });

    $scope.setImage = function(imageUrl) {
      $scope.mainImageUrl = imageUrl;
    };
  }]);
phonecatControllers.controller('ClientListCtrl', ['$scope', 'Client',
  function($scope, Client) {
    $scope.clients = Client.query();
    $scope.orderProp = 'age';
  }]);
phonecatControllers.controller('ClientDetailCtrl', ['$scope', '$routeParams', 'Client',
  function($scope, $routeParams, Client) {
    $scope.client = Client.get({clientId: $routeParams.clientId}, function(client) {
      //$scope.mainImageUrl = client.images[0];
      $scope.image = client.imageUrl;
      $scope.name = client.first_name + " " + client.last_name;
    });

    $scope.setImage = function(imageUrl) {
      //$scope.mainImageUrl = imageUrl;
    };
  }]);

phonecatControllers.controller('ModalDemoCtrl', function ($scope, $uibModal, $log) {

  $scope.items = ['item1', 'item2', 'item3'];

  $scope.animationsEnabled = true;

  $scope.open = function (size) {

    var modalInstance = $uibModal.open({
      animation: $scope.animationsEnabled,
      templateUrl: 'modals/workout-modal.html',
      controller: 'ModalInstanceCtrl',
      size: size,
      resolve: {
        items: function () {
          return $scope.items;
        }
      }
    });

    modalInstance.result.then(function (selectedItem) {
      $scope.selected = selectedItem;
    }, function () {
      $log.info('Modal dismissed at: ' + new Date());
    });
  };

  $scope.toggleAnimation = function () {
    $scope.animationsEnabled = !$scope.animationsEnabled;
  };

});

// Please note that $modalInstance represents a modal window (instance) dependency.
// It is not the same as the $uibModal service used above.

phonecatControllers.controller('ModalInstanceCtrl', function ($scope, $uibModalInstance, items) {

  $scope.items = items;
  $scope.selected = {
    item: $scope.items[0]
  };

  $scope.ok = function () {
    $uibModalInstance.close($scope.selected.item);
  };

  $scope.cancel = function () {
    $uibModalInstance.dismiss('cancel');
  };
})


phonecatControllers.controller('ClientTableCtrl', ['$scope', '$http',
  function($scope, $http) {
    $http.get('./clients/client_list.json').success(function(data){
      $scope.clients = data;
    });
  }
]);

phonecatControllers.controller('LoginCtrl', ['$scope',
  function($scope) {
    $scope.name = '';
    $scope.username = '';
    $scope.password = '';
    $scope.email = '';

    $scope.register = function () {

      var user = new Parse.User();
      user.set("username", $scope.username);
      user.set("password", $scope.password);
      user.set("email", $scope.email);
      user.set("name", $scope.name);
      user.save();

      var Coach = new Parse.Object.extend("Coach");
      var coach = new Coach();
      coach.set("name", $scope.name);
      coach.set("userId", user);
      coach.save();

      user.signUp(null, {
        success: function(user) {
        },
        error: function(user, error) {
          alert("Error: " + error.code + " " + error.message);
        }
      });
    }

    $scope.login = function() {
      Parse.User.logIn($scope.username, $scope.password, {
        success: function(user) {
          alert("Success");
        },
        error: function(user, error) {
          alert("Error: " + error.code + " " + error.message);
        }
      });
    }

    $scope.login_fb = function () {
      Parse.FacebookUtils.logIn(null, {
        success: function (user) {
          if (!user.existed()) {
            alert("User signed up and logged in through Facebook!");
          } else {
            alert("User logged in through Facebook!");
            alert(Parse.User.current());
          }
        },
        error: function (user, error) {
          alert("User cancelled the Facebook login or did not fully authorize.");
          alert(Parse.User.current());
        }
      });
    }
  }
]);



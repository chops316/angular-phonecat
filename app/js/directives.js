'use strict';
/* Directives */
var phonecatDirectives = angular.module('phonecatDirectives', []);

phonecatDirectives.directive('schedulr', function() {
    return {
        restrict: 'E',
        templateUrl: 'partials/modals/schedulr.html',
        controller: function () {

        },
        controllerAs: 'SchedulrCtrl'
    };
});

phonecatDirectives.directive('modalHabit', function() {
    return {
        restrict: 'E',
        templateUrl: 'partials/modals/habit-modal.html'
    };
});

phonecatDirectives.directive('modalSurvey',function() {
    return {
        restrict: 'E',
        templateUrl: 'partials/modals/survey-modal.html'
    };
});

phonecatDirectives.directive('modalWorkout', function() {
    return {
        restrict: 'E',
        templateUrl: 'partials/modals/workout-modal.html'
    };
});

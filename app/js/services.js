'use strict';

/* Services */

var phonecatServices = angular.module('phonecatServices', ['ngResource']);

phonecatServices.factory('Phone', ['$resource',
  function($resource){
    return $resource('phones/:phoneId.json', {}, {
      query: {method:'GET', params:{phoneId:'phones'}, isArray:true}
    });
  }]);
phonecatServices.factory('Client', ['$resource',
  function($resource){
    return $resource('clients/:clientId.json', {}, {
      query: {method:'GET', params:{clientId:'clients'}, isArray:true}
    });
  }]);

//phonecatServices.factory('ClientTable', ['$http',
//  function($http){
//    var clients = this;
//    $http.get('clients/client_list.json').success(function(data) {
//      clients = data;
//    });
//}]);

